//Variables
const form              = document.querySelector('#form');
const tweetsContainer   = document.querySelector('#tweets-container');
let tweets = [];

//EventListeners
init();


//functions
function init() {
    form.addEventListener('submit', addTweet);

    addEventListener('DOMContentLoaded', () => {
        tweets = JSON.parse( localStorage.getItem('tweets') );

        if( tweets === null) {
            tweets = [];
        } else {
            showTweets();
        }
    });
}

function addTweet(e) {
    e.preventDefault();
    
    const tweet = document.querySelector('#tweet').value;

    if(tweet === '') {
        showMessage('error', 'El Tweet es obligatorio.');
        return;
    }

    const tweetObj = {
        id: Date.now(),
        name: tweet
    }

    tweets = [...tweets, tweetObj];

    saveTweet();

    form.reset();

    showMessage(null, 'El tweet se ha agregado exitosamente.');

    showTweets();
}

function saveTweet() {
    localStorage.setItem('tweets', JSON.stringify(tweets))
}

function deleteTweet(id) {
    tweets = tweets.filter(tweet => id !== tweet.id);
    
    saveTweet();

    showTweets();
    
}

function showMessage(type, msg) {
    const p = document.createElement('p');
    p.textContent = msg;
    p.classList.add('alert');

    if(type === 'error') {
        p.classList.add('danger');
    } else {
        p.classList.add('success');
    }

    const contentido = document.querySelector('#contenido');
    contentido.appendChild(p);

    setTimeout(() => {
        p.remove();
    },2000)
}

function showTweets() {
    if( tweets.length > 0 ) {
        cleanTweetsContainer();
        tweets.forEach(tweet => {
            const a = document.createElement('a');
            a.classList.add('borrar-tweet');
            a.textContent = 'X';
            a.onclick = () => {
                deleteTweet(tweet.id);
            }

            const li = document.createElement('li');
            li.textContent = tweet.name;

            li.appendChild(a);

            tweetsContainer.append(li);
        });
    }
}

function cleanTweetsContainer() {
    while(tweetsContainer.firstElementChild) {
        tweetsContainer.removeChild(tweetsContainer.firstElementChild);
    }
}